# cool_QRCode

#### 介绍
一个能生成gif二维码的程序，使用了myqr库。


#### 安装教程

需要提前安装myqr库

#### 使用说明

在source文件夹下放置图片文件，在ide中打开main.py，修改二维码的内容，然后运行即可。

#### 参与贡献

个人网站：longjin666.top


