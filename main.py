#运行本程序前，需要安装myqr库

from MyQR import myqr
import os
import pprint


words = 'http://longjin666.top'     #可以是字符串也可以是网址（前面要加http(s)://)

error_files = []

for root, dirs, files in os.walk('source/'):
    for f in files:
        print('正在处理：' + f)
        background = os.path.join(root, f)  # 背景图片
        savename = 'result_' + f  # 保存的文件名，格式可以是jpg,png,bmp,gif
        try:

            myqr.run(
                words=words,
                version=1,  # 设置容错率为最高
                level='H',  # 控制纠错水平，范围是L、M、Q、H，从左到右依次升高
                picture=background,
                colorized=True,  # 彩色二维码
                contrast=1.0,  # 图片对比度，1.0为原始图片，数值越小对比度越低，反之越高
                brightness=1.0,  # 调节图片亮度，用法如上
                save_name=savename,
                save_dir='result/'
            )
            print('输出成功：', savename)
        except Exception as e:
            print('出现错误：', background)
            error_files.append(background)

print('处理完成！')
if error_files:
    print('以下是出现错误的文件：')
    pprint.pprint(error_files)


